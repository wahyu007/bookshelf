const { nanoid } = require('nanoid');
const books = require('./books');

const getAllBooksHandler = (request, h) => {
  const newBooks = [];

  if (books.length !== 0) {
    books.forEach((element) => {
      const obj = {
        id: element.id,
        name: element.name,
        publisher: element.publisher,
      };
      newBooks.push(obj);
    });
  }

  const response = h.response({
    status: 'success',
    data: {
      books: newBooks,
    },
  });

  response.code(200);
  return response;
};

const addBookHandler = (request, h) => {
  const {
    name,
    year,
    author,
    summary,
    publisher,
    pageCount,
    readPage,
    reading,
  } = request.payload;

  const id = nanoid(16);
  const inserteddAt = new Date().toISOString();
  const updatedAt = inserteddAt;
  const finished = readPage === pageCount;

  if (name === null) {
    const response = h.response({
      status: 'fail',
      message: 'Catatan gagal ditambahkan',
    });

    response.code(400);
    return response;
  }

  if (readPage >= pageCount) {
    const response = h.response({
      status: 'fail',
      message: 'Catatan gagal ditambahkan',
    });

    response.code(400);
    return response;
  }

  const newBook = {
    id,
    name,
    year,
    author,
    summary,
    publisher,
    pageCount,
    readPage,
    finished,
    reading,
    inserteddAt,
    updatedAt,
  };

  books.push(newBook);

  const isSuccess = books.filter((note) => note.id === id).length > 0;

  if (isSuccess) {
    const response = h.response({
      status: 'success',
      message: 'Buku berhasil ditambahkan',
      data: {
        noteId: id,
      },
    });

    response.code(201);
    return response;
  }

  const response = h.response({
    status: 'fail',
    message: 'Buku gagal ditambahkan',
  });

  response.code(500);
  return response;
};

const getBookByIdHandler = (request, h) => {
  const { id } = request.params;

  const book = books.filter((n) => n.id === id)[0];

  if (book !== undefined) {
    return {
      status: 'success',
      data: {
        book,
      },
    };
  }

  const response = h.response({
    status: 'fail',
    message: 'Buku tidak ditemukan',
  });

  response.code(404);
  return response;
};

module.exports = { addBookHandler, getAllBooksHandler, getBookByIdHandler };
